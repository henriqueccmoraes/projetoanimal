/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package projetoanimal;

import java.util.Scanner;

/**
 *
 * @author user
 */
public class ProjetoAnimal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Scanner teclado = new Scanner(System.in);
        int escolha;

        Mamifero m1 = new Mamifero();
        m1.membros = 4;

        Ave a1 = new Ave();
        a1.membros = 2;

        Reptil r1 = new Reptil();

        Mamifero m[] = new Mamifero[2];
        m[0] = new Mamifero("pelos");
        Ave a[] = new Ave[2];
        a[0] = new Ave("penas");
        Reptil r[] = new Reptil[2];
        r[0] = new Reptil("escamas");
        System.out.println("Informe qual é o tipo de animal deseja conhecer ?");
        System.out.println("Digite 1 para mamifero\n" + "Digite 2 para aves\n" + "Digite 3 para reptil");

        escolha = teclado.nextInt();
        if (escolha == 1) {
            System.out.println("Os mamiferos, com algumas exceções, possuem o corpo coberto de " + m[0].getTipoPele() + " e possuem " + m1.membros + " membros para se locomover");
        }
        if (escolha == 2) {
            System.out.println("As aves normalmente possuem o corpo coberto de" + a[0].getProtPele() + " e possuem " + a1.membros + " asas para voarem");
        }
        if (escolha == 3) {
            System.out.print("Os repteis possuem o corpo coberto de " + r[0].getPele() + ", se locomovem ");
            r1.locomover();
            System.out.print("E se alimentam ");
            r1.alimentar();
        }

        // Mamifero m = new Mamifero();
        //m.locomover();
        //m.alimentar();
        //m.setMembros(4);
        //m.setTipoPele("Pelos");
        //System.out.println("Um mamífero cachorro possui o corpo coberto de " + m.getTipoPele() + " possui " + m.getMembros() + " membros" );
        //System.out.print("Se locomove ");
        //m.locomover();
        //System.out.print("Se alimenta ");
        //m.alimentar();
    }

}
